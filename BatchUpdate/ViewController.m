//
//  ViewController.m
//  BatchUpdate
//
//  Created by Alexander Korniyets on 4/26/16.
//  Copyright © 2016 Alexander Korniyets. All rights reserved.
//

#import "ViewController.h"
#import "UBCollectionViewUpdatesBuilder.h"
#import "CollectionViewCell.h"

@interface Row : NSObject<UBCollectionRowItemProtocol>

@property(copy, nonatomic) NSNumber *number;
@property(copy, nonatomic) NSString *UUID;
@property(copy, nonatomic) NSString *type;

@end

@implementation Row

- (NSString *)description
{
    NSArray *components = @[[self UUID], [self type], [self number]];
    return [components componentsJoinedByString:@"\n"];;
}

@end

@interface Section : NSObject<UBCollectionSectionItemProtocol>

@property(assign, nonatomic) NSUInteger rowsCount;
@property(copy, nonatomic) NSString *UUID;
@property(copy, nonatomic) NSArray *rows;

@end

@implementation Section

- (NSArray<id<UBCollectionRowItemProtocol>> *)items
{
    return [self rows];
}

- (NSString *)description
{
    return [self UUID];
}

@end

@interface ViewController ()<UICollectionViewDataSource>

@property(strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property(assign, nonatomic) NSUInteger rowsCount;
@property(assign, nonatomic) NSUInteger sectionsCount;

@property(strong, nonatomic) NSMutableArray<Section *> *sections;
@property(strong, nonatomic) NSOperationQueue *updateQueue;
@property(strong, nonatomic) NSOperationQueue *queue;

@property(strong, nonatomic) UBCollectionViewUpdatesBuilder *updater;

@end

static NSUInteger rowsCount = 5;

@implementation ViewController

- (NSArray<Row *> *)rowsWithStartIndex:(NSInteger)startIndex count:(NSInteger)count
{
    NSMutableArray *resultRows = [NSMutableArray new];
    
    for (NSInteger i = 0; i < count; i++)
    {
        NSInteger index = startIndex + i;
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSNumber *number = @(index);
        NSString *type = @"default";
        
        Row *currentRow = [[Row alloc] init];
        [currentRow setType:type];
        [currentRow setUUID:uuid];
        [currentRow setNumber:number];
        [resultRows addObject:currentRow];
    }
    
    return resultRows;
}

- (NSArray<Section *> *)sectionsWithCount:(NSInteger)count
{
    NSMutableArray *resultSections = [NSMutableArray new];
    
    for (NSInteger i = 0; i < count; i++)
    {
        NSString *uuid = [[NSUUID UUID] UUIDString];
        
        Section *section = [[Section alloc] init];
        [section setUUID:uuid];
        [section setRowsCount:rowsCount];
        [section setRows:[self rowsWithStartIndex:(i * 100) count:rowsCount]];
        [resultSections addObject:section];
    }
    
    return resultSections;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    Section *section = [[Section alloc] init];
    [section setUUID:[[NSUUID UUID] UUIDString]];
    [section setRowsCount:rowsCount];
    
    [section setRows:[self rowsWithStartIndex:0 count:rowsCount]];
    
    Section *section2 = [[Section alloc] init];
    [section2 setRowsCount:rowsCount];
    [section2 setUUID:[[NSUUID UUID] UUIDString]];
    [section2 setRows:[self rowsWithStartIndex:0 count:rowsCount]];
    
    Section *section3 = [[Section alloc] init];
    [section3 setRowsCount:rowsCount];
    [section3 setUUID:[[NSUUID UUID] UUIDString]];
    [section3 setRows:[self rowsWithStartIndex:0 count:rowsCount]];
    
    Section *section4 = [[Section alloc] init];
    [section4 setRowsCount:rowsCount];
    [section4 setUUID:[[NSUUID UUID] UUIDString]];
    [section4 setRows:[self rowsWithStartIndex:0 count:rowsCount]];
    
    Section *section5 = [[Section alloc] init];
    [section5 setRowsCount:rowsCount];
    [section5 setUUID:[[NSUUID UUID] UUIDString]];
    [section4 setRows:[self rowsWithStartIndex:0 count:rowsCount]];
    
    NSArray<Section *> *sections = [self sectionsWithCount:5];
    
    [self setSections:[NSMutableArray arrayWithArray:sections]];
    
    [[self collectionView] reloadData];
    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[self sections] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    Section *section = [[self sections] objectAtIndex:[indexPath section]];
    Row *row = [[section rows] objectAtIndex:[indexPath row]];
    
    [cell setNumber:[[row number] integerValue]];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[[self sections] objectAtIndex:section] rows] count];
}


- (IBAction)add:(id)sender
{
    [self addRemoveMoveAction];
}

- (void)addRemoveMoveAction
{
    NSArray *oldSections = [[self sections] copy];
    NSMutableArray *newSections = [[NSMutableArray alloc] initWithArray:oldSections];
    
    [newSections removeObjectAtIndex:0];
    
    NSInteger prelastIndex = [newSections count] - 2;
    Section *prelastSession = [newSections objectAtIndex:prelastIndex];
    [newSections removeObjectAtIndex:prelastIndex];
    [newSections addObject:prelastSession];
    
    NSInteger lastIndex = [newSections count] - 1;
    
    Section *section = [[Section alloc] init];
    [section setUUID:[[NSUUID UUID] UUIDString]];
    [section setRowsCount:rowsCount];
    
    [newSections addObject:section];
    
    
    
    UBCollectionViewUpdatesBuilder *builder = [[UBCollectionViewUpdatesBuilder alloc] init];
    [self setUpdater:builder];
    
    [builder performBatchUpdatesForCollectionView:[self collectionView]
                                     fromOldState:[self sections]
                                       toNewState:newSections
                        withDataSourceUpdateBlock:^
     {
         [self setSections:newSections];
     }
                                       completion:^(BOOL finished)
     {
         NSLog(@"finish");
     }];
}

- (void)testStayOnPlaceAction
{
    NSArray *oldSections = [self sections];
    
    Section *oldSection = [[self sections] firstObject];
    Section *newSection = [[Section alloc] init];
    [newSection setUUID:[oldSection UUID]];

    NSArray *rows = [oldSection rows];
    
    NSMutableArray<Row *> *newRows = [NSMutableArray new];
    
    [rows enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [newRows addObject:obj];
    }];
    
    [newSection setRows:newRows];
    
    NSMutableArray *newSections = [[NSMutableArray alloc] initWithArray:oldSections];
    [newSections removeObjectAtIndex:0];
    [newSections insertObject:newSection atIndex:0];
    
    UBCollectionViewUpdatesBuilder *builder = [[UBCollectionViewUpdatesBuilder alloc] init];
    [self setUpdater:builder];
    
    [builder performBatchUpdatesForCollectionView:[self collectionView]
                                     fromOldState:oldSections
                                       toNewState:newSections
                        withDataSourceUpdateBlock:^
     {
         [self setSections:newSections];
     }
                                       completion:^(BOOL finished)
     {
         NSLog(@"finish");
     }];
}

- (IBAction)delete:(id)sender
{
    [self testStayOnPlaceAction];
}


- 

@end
