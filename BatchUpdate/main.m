//
//  main.m
//  BatchUpdate
//
//  Created by Alexander Korniyets on 4/26/16.
//  Copyright © 2016 Alexander Korniyets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
