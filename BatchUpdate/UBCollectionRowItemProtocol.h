//
//  UBCollectionRowItemProtocol.h
//  uBooks
//
//  Created by Alexander Korniyets on 5/19/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UBCollectionRowItemProtocol <NSObject>

- (NSString *)UUID;
- (NSString *)type;

@end

@protocol UBCollectionSectionItemProtocol <NSObject>

- (NSString *)UUID;
- (NSArray<id<UBCollectionRowItemProtocol>> *)items;

@end