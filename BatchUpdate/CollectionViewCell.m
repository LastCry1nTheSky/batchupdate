//
//  CollectionViewCell.m
//  BatchUpdate
//
//  Created by Alexander Korniyets on 4/26/16.
//  Copyright © 2016 Alexander Korniyets. All rights reserved.
//

#import "CollectionViewCell.h"

@interface CollectionViewCell ()

@property(weak, nonatomic) IBOutlet UILabel *numberLabel;

@end

@implementation CollectionViewCell

- (void)setNumber:(NSUInteger)number
{
    [[self numberLabel] setText:[NSString stringWithFormat:@"%ld", number]];
}

@end
