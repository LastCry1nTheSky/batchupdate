//
//  UBCollectionViewUpdatesBuilder.m
//  uBooks
//
//  Created by Alexander Korniyets on 5/19/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "UBCollectionViewUpdatesBuilder.h"

typedef void (^SystemArrayEnumerationBlock)(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop);
typedef BOOL (^SystemPassingTestBlock)(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop);

@interface UBMoveSectionParams : NSObject

@property(assign, nonatomic) NSInteger fromIndex;
@property(assign, nonatomic) NSInteger toIndex;

@end

@implementation UBMoveSectionParams


@end

@implementation UBCollectionViewUpdatesBuilder

- (void)performBatchUpdatesForCollectionView:(UICollectionView *)collectionView
                                fromOldState:(NSArray<id<UBCollectionSectionItemProtocol>> *)oldState
                                  toNewState:(NSArray<id<UBCollectionSectionItemProtocol>> *)newState
                   withDataSourceUpdateBlock:(UBBatchUpdateDataSourceBlock)dataSourceUpdateBlock
                                  completion:(UBBatchUpdateCompletionBlock)completion
{
    NSMutableIndexSet *removedSectionsSet = [[NSMutableIndexSet alloc] init];
    
    SystemArrayEnumerationBlock findRemovedSectionsEnumerationBlock = ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
    {
        id<UBCollectionSectionItemProtocol> section = obj;
        NSString *searchedUUID = [section UUID];
        
        SystemPassingTestBlock findMatchedSectionBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            id<UBCollectionSectionItemProtocol> section = obj;
            
            return [[section UUID] isEqualToString:searchedUUID];
        };
        
        NSUInteger resultIndex = [newState indexOfObjectPassingTest:findMatchedSectionBlock];
        
        if (resultIndex == NSNotFound)
        {
            [removedSectionsSet addIndex:idx];
        }
    };
    
    [oldState enumerateObjectsUsingBlock:findRemovedSectionsEnumerationBlock];
    
    NSMutableIndexSet *insertedSectionsSet = [[NSMutableIndexSet alloc] init];
    
    SystemArrayEnumerationBlock findInsertedSectionsEnumerationBlock = ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
    {
        id<UBCollectionSectionItemProtocol> section = obj;
        NSString *searchedUUID = [section UUID];
        
        SystemPassingTestBlock findMatchedSectionBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            id<UBCollectionSectionItemProtocol> section = obj;
            
            return [[section UUID] isEqualToString:searchedUUID];
        };
        
        NSUInteger resultIndex = [oldState indexOfObjectPassingTest:findMatchedSectionBlock];
        
        if (resultIndex == NSNotFound)
        {
            [insertedSectionsSet addIndex:idx];
        }
    };
    
    [newState enumerateObjectsUsingBlock:findInsertedSectionsEnumerationBlock];
    
    NSMutableArray *newStateBeforeInsert = [[NSMutableArray alloc] initWithArray:newState];
    [newStateBeforeInsert removeObjectsAtIndexes:insertedSectionsSet];
    
    NSMutableArray *oldStateAfterRemove = [[NSMutableArray alloc] initWithArray:oldState];
    [oldStateAfterRemove removeObjectsAtIndexes:removedSectionsSet];
    
    NSMutableArray<UBMoveSectionParams *> *moves = [[NSMutableArray alloc] init];
    
    NSMutableArray *stayedSectionsFromOldState = [[NSMutableArray alloc] init];
    NSMutableArray *stayedSectionsFromNewState = [[NSMutableArray alloc] init];
    
    if ([newStateBeforeInsert count] == [oldStateAfterRemove count])
    {
        for (NSUInteger i = 0; i < [oldStateAfterRemove count]; i++)
        {
            id<UBCollectionSectionItemProtocol> oldSection = [oldStateAfterRemove objectAtIndex:i];
            id<UBCollectionSectionItemProtocol> newSection = [newStateBeforeInsert objectAtIndex:i];
            
            if (![[oldSection UUID] isEqualToString:[newSection UUID]])
            {
                NSString *searchedUUID = [oldSection UUID];
                
                SystemPassingTestBlock findMatchedSectionBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                {
                    id<UBCollectionSectionItemProtocol> section = obj;
                    
                    return [[section UUID] isEqualToString:searchedUUID];
                };

                NSUInteger indexInInOldState = [oldState indexOfObjectPassingTest:findMatchedSectionBlock];
                NSUInteger indexInInNewState = [newState indexOfObjectPassingTest:findMatchedSectionBlock];
                
                if ((indexInInOldState == NSNotFound) || (indexInInNewState == NSNotFound))
                {
                    NSAssert(NO, @"ololo");
                }

                UBMoveSectionParams *moveParams = [[UBMoveSectionParams alloc] init];
                [moveParams setFromIndex:indexInInOldState];
                [moveParams setToIndex:indexInInNewState];
                [moves addObject:moveParams];
            }
            else
            {
                [stayedSectionsFromOldState addObject:oldSection];
                [stayedSectionsFromNewState addObject:newSection];
            }
        }
    }
    else
    {
        NSAssert(NO, @"ERROR BLEAD");
    }
    
    // handle stayed sections
    
    NSMutableArray<NSIndexPath *> *insertedIndexPaths = [[NSMutableArray alloc] init];
    NSMutableArray<NSIndexPath *> *removedIndexPaths = [[NSMutableArray alloc] init];
    
    for (NSUInteger i = 0; i < [stayedSectionsFromOldState count]; i++)
    {
        id<UBCollectionSectionItemProtocol> oldSection = [oldStateAfterRemove objectAtIndex:i];
        id<UBCollectionSectionItemProtocol> newSection = [newStateBeforeInsert objectAtIndex:i];
        
        NSString *searchedUUID = [oldSection UUID];
        
        SystemPassingTestBlock findMatchedSectionBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            id<UBCollectionSectionItemProtocol> section = obj;
            
            return [[section UUID] isEqualToString:searchedUUID];
        };
        
        NSUInteger indexInInOldState = [oldState indexOfObjectPassingTest:findMatchedSectionBlock];
        NSUInteger indexInInNewState = [newState indexOfObjectPassingTest:findMatchedSectionBlock];
        
        NSArray<NSIndexPath *> *insertedIndexPathsInSection = [self insertedPathsForOldState:oldSection
                                                                    oldSectionIndex:indexInInOldState
                                                                           newState:newSection
                                                                    newSectionIndex:indexInInNewState];
        
        NSArray<NSIndexPath *> *removedIndexPathsInSection = [self removedPathsForOldState:oldSection
                                                                    oldSectionIndex:indexInInOldState
                                                                           newState:newSection
                                                                    newSectionIndex:indexInInNewState];
        [insertedIndexPaths addObjectsFromArray:insertedIndexPathsInSection];
        [removedIndexPaths addObjectsFromArray:removedIndexPathsInSection];
    }
    
    NSLog(@"");
    
    void (^reloadUpdatesCompletionBlock)(BOOL) = ^(BOOL finished)
    {
        completion(finished);
    };
    
    void (^reloadUpdatesBlock)(void) = ^
    {
        
    };
    
    void (^reorderUpdatesCompletionBlock)(BOOL) = ^(BOOL finished)
    {
        [collectionView performBatchUpdates:reloadUpdatesBlock
                                 completion:reloadUpdatesCompletionBlock];
    };
    
    void (^reorderUpdatesBlock)(void) = ^
    {
        dataSourceUpdateBlock();
        
        [collectionView deleteSections:removedSectionsSet];
        [collectionView insertSections:insertedSectionsSet];
        
        for (UBMoveSectionParams *move in moves)
        {
            [collectionView moveSection:[move fromIndex] toSection:[move toIndex]];
        }
    };

    [collectionView performBatchUpdates:reorderUpdatesBlock
                             completion:reorderUpdatesCompletionBlock];
}

- (NSArray<NSIndexPath *> *)insertedPathsForOldState:(id<UBCollectionSectionItemProtocol>)oldState
                                   oldSectionIndex:(NSInteger)oldIndex
                                          newState:(id<UBCollectionSectionItemProtocol>)newState
                                   newSectionIndex:(NSInteger)newIndex
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSMutableIndexSet *removedRowsSet = [[NSMutableIndexSet alloc] init];
    
    SystemArrayEnumerationBlock findRemovedRowsEnumerationBlock = ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
    {
        id<UBCollectionRowItemProtocol> currentRow = obj;
        NSString *searchedUUID = [currentRow UUID];
        
        SystemPassingTestBlock findMatchedRowsBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            id<UBCollectionRowItemProtocol> row = obj;
            
            return [[row UUID] isEqualToString:searchedUUID];
        };
        
        NSUInteger resultIndex = [[newState items] indexOfObjectPassingTest:findMatchedRowsBlock];
        
        if (resultIndex == NSNotFound)
        {
            [removedRowsSet addIndex:idx];
            [result addObject:[NSIndexPath indexPathForRow:idx inSection:oldIndex]];
        }
    };
    
    [[oldState items] enumerateObjectsUsingBlock:findRemovedRowsEnumerationBlock];
    
    return result;
}

- (NSArray<NSIndexPath *> *)removedPathsForOldState:(id<UBCollectionSectionItemProtocol>)oldState
                                  oldSectionIndex:(NSInteger)oldIndex
                                         newState:(id<UBCollectionSectionItemProtocol>)newState
                                  newSectionIndex:(NSInteger)newIndex
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSMutableIndexSet *insertedRowsSet = [[NSMutableIndexSet alloc] init];
    
    SystemArrayEnumerationBlock findRemovedRowsEnumerationBlock = ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
    {
        id<UBCollectionRowItemProtocol> currentRow = obj;
        NSString *searchedUUID = [currentRow UUID];
        
        SystemPassingTestBlock findMatchedRowsBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            id<UBCollectionRowItemProtocol> row = obj;
            
            return [[row UUID] isEqualToString:searchedUUID];
        };
        
        NSUInteger resultIndex = [[oldState items] indexOfObjectPassingTest:findMatchedRowsBlock];
        
        if (resultIndex == NSNotFound)
        {
            [insertedRowsSet addIndex:idx];
            [result addObject:[NSIndexPath indexPathForRow:idx inSection:newIndex]];
        }
    };
    
    [[newState items] enumerateObjectsUsingBlock:findRemovedRowsEnumerationBlock];
    
    return result;
}

@end
