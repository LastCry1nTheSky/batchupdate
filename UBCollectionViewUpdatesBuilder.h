//
//  UBCollectionViewUpdatesBuilder.h
//  uBooks
//
//  Created by Alexander Korniyets on 5/19/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UBAllBooksCollectionViewDataSourceCellObject.h"
#import "UBBookItemSection.h"

typedef void (^UBBatchUpdateDataSourceBlock)(void);
typedef void (^UBBatchUpdateCompletionBlock)(BOOL finished);

typedef enum : NSUInteger
{
    UBUpdateActionInsert,
    UBUpdateActionRemove,
    UBUpdateActionMove,
} UBUpdateActionType;

@interface UBSectionUpdate : NSObject

@property(assign, nonatomic) UBUpdateActionType type;
@property(copy, nonatomic) NSNumber *fromIndex;
@property(copy, nonatomic) NSNumber *toIndex;

@end

@interface UBRowUpdate : NSObject

@property(assign, nonatomic) UBUpdateActionType type;
@property(copy, nonatomic) NSIndexPath *fromIndexPath;
@property(copy, nonatomic) NSIndexPath *toIndexPath;

@end

@interface UBBatchUpdates : NSObject

@property(copy, nonatomic) NSArray<UBSectionUpdate *> *sectionUpdates;
@property(copy, nonatomic) NSArray<UBRowUpdate *> *rowsUpdates;

+ (UBBatchUpdates *)updatesByFilteringSectionBlock:(BOOL(^)(UBSectionUpdate *section))sectionBlock
                                         itemBlock:(BOOL(^)(UBRowUpdate *item))itemBlock;

@end

@interface UBCollectionViewUpdatesBuilder : NSObject

- (void)performBatchUpdatesForCollectionView:(UICollectionView *)collectionView
                                fromOldState:(NSArray<id<UBCollectionSectionItemProtocol>> *)oldState
                                  toNewState:(NSArray<id<UBCollectionSectionItemProtocol>> *)newState
                   withDataSourceUpdateBlock:(UBBatchUpdateDataSourceBlock)dataSourceUpdateBlock
                                  completion:(UBBatchUpdateCompletionBlock)completion;

- (UBBatchUpdates *)updatesFromOldState:(NSArray<UBBookItemSection *> *)oldState
                             toNewState:(NSArray<UBBookItemSection *> *)newState;

- (NSArray<UBBookItemSection *> *)newStateFromOldState:(NSArray<UBBookItemSection *> *)oldState
                                           withUpdates:(UBBatchUpdates *)updates;

@end
