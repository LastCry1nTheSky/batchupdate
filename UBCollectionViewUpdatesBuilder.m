//
//  UBCollectionViewUpdatesBuilder.m
//  uBooks
//
//  Created by Alexander Korniyets on 5/19/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "UBCollectionViewUpdatesBuilder.h"

typedef void (^SystemArrayEnumerationBlock)(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop);
typedef BOOL (^SystemPassingTestBlock)(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop);

@interface UBMoveSectionParams : NSObject

@property(assign, nonatomic) NSInteger fromIndex;
@property(assign, nonatomic) NSInteger toIndex;

@end

@implementation UBMoveSectionParams


@end

@implementation UBCollectionViewUpdatesBuilder

- (void)performBatchUpdatesForCollectionView:(UICollectionView *)collectionView
                                fromOldState:(NSArray<id<UBCollectionSectionItemProtocol>> *)oldState
                                  toNewState:(NSArray<id<UBCollectionSectionItemProtocol>> *)newState
                   withDataSourceUpdateBlock:(UBBatchUpdateDataSourceBlock)dataSourceUpdateBlock
                                  completion:(UBBatchUpdateCompletionBlock)completion
{
    NSMutableIndexSet *removedSectionsSet = [[NSMutableIndexSet alloc] init];
    
    SystemArrayEnumerationBlock findRemovedSectionsEnumerationBlock = ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
    {
        id<UBCollectionSectionItemProtocol> section = obj;
        NSString *searchedUUID = [section UUID];
        
        SystemPassingTestBlock findMatchedSectionBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            id<UBCollectionSectionItemProtocol> section = obj;
            
            return [[section UUID] isEqualToString:searchedUUID];
        };
        
        NSUInteger resultIndex = [newState indexOfObjectPassingTest:findMatchedSectionBlock];
        
        if (resultIndex == NSNotFound)
        {
            [removedSectionsSet addIndex:idx];
        }
    };
    
    [oldState enumerateObjectsUsingBlock:findRemovedSectionsEnumerationBlock];
    
    NSMutableIndexSet *insertedSectionsSet = [[NSMutableIndexSet alloc] init];
    
    SystemArrayEnumerationBlock findInsertedSectionsEnumerationBlock = ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
    {
        id<UBCollectionSectionItemProtocol> section = obj;
        NSString *searchedUUID = [section UUID];
        
        SystemPassingTestBlock findMatchedSectionBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            id<UBCollectionSectionItemProtocol> section = obj;
            
            return [[section UUID] isEqualToString:searchedUUID];
        };
        
        NSUInteger resultIndex = [oldState indexOfObjectPassingTest:findMatchedSectionBlock];
        
        if (resultIndex == NSNotFound)
        {
            [insertedSectionsSet addIndex:idx];
        }
    };
    
    [newState enumerateObjectsUsingBlock:findInsertedSectionsEnumerationBlock];
    
    NSMutableArray *newStateBeforeInsert = [[NSMutableArray alloc] initWithArray:newState];
    [newStateBeforeInsert removeObjectsAtIndexes:insertedSectionsSet];
    
    NSMutableArray *oldStateAfterRemove = [[NSMutableArray alloc] initWithArray:oldState];
    [oldStateAfterRemove removeObjectsAtIndexes:removedSectionsSet];
    
    NSMutableArray<UBMoveSectionParams *> *moves = [[NSMutableArray alloc] init];
    
    if ([newStateBeforeInsert count] == [oldStateAfterRemove count])
    {
        for (NSUInteger i = 0; i < [oldStateAfterRemove count]; i++)
        {
            id<UBCollectionSectionItemProtocol> oldSection = [oldStateAfterRemove objectAtIndex:i];
            id<UBCollectionSectionItemProtocol> newSection = [newStateBeforeInsert objectAtIndex:i];
            
            if (![[oldSection UUID] isEqualToString:[newSection UUID]])
            {
                NSString *searchedUUID = [oldSection UUID];
                
                SystemPassingTestBlock findMatchedSectionBlock = ^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                {
                    id<UBCollectionSectionItemProtocol> section = obj;
                    
                    return [[section UUID] isEqualToString:searchedUUID];
                };

                NSUInteger indexInInOldState = [oldState indexOfObjectPassingTest:findMatchedSectionBlock];
                NSUInteger indexInInNewState = [newState indexOfObjectPassingTest:findMatchedSectionBlock];
                
                if ((indexInInOldState == NSNotFound) || (indexInInNewState == NSNotFound))
                {
                    NSAssert(NO, @"ololo");
                }

                UBMoveSectionParams *moveParams = [[UBMoveSectionParams alloc] init];
                [moveParams setFromIndex:indexInInOldState];
                [moveParams setToIndex:indexInInNewState];
                [moves addObject:moveParams];
            }
        }
    }
    else
    {
        NSAssert(NO, @"ERROR BLEAD");
    }
    
    void (^reloadUpdatesCompletionBlock)(BOOL) = ^(BOOL finished)
    {
        completion(finished);
    };
    
    void (^reloadUpdatesBlock)(void) = ^
    {
        
    };
    
    void (^reorderUpdatesCompletionBlock)(BOOL) = ^(BOOL finished)
    {
        [collectionView performBatchUpdates:reloadUpdatesBlock
                                 completion:reloadUpdatesCompletionBlock];
    };
    
    void (^reorderUpdatesBlock)(void) = ^
    {
    };

    [collectionView performBatchUpdates:reorderUpdatesBlock
                             completion:reorderUpdatesCompletionBlock];
}

//- (NSArray<NSIndexPath *> *)insertedIndexPathForSection:(id)section toSection:(id)toSection fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
//    
//}

@end
